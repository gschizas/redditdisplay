int number=0;
int baudRate=9600;
int firstDidgitPin=2;
int lastDidgitPin=5;
int firstSegmentPin=6;
int lastSegmentPin=12;
int pow10[4] = {1000,100,10,1};
const byte aSegments[11][8] = {
//  A     B     C     D     E     F     G
{ HIGH, HIGH, HIGH, HIGH, HIGH, HIGH,  LOW }, // 0
{  LOW, HIGH, HIGH,  LOW,  LOW,  LOW,  LOW }, // 1
{ HIGH, HIGH,  LOW, HIGH, HIGH,  LOW, HIGH }, // 2
{ HIGH, HIGH, HIGH, HIGH,  LOW,  LOW, HIGH }, // 3
{  LOW, HIGH, HIGH,  LOW,  LOW, HIGH, HIGH }, // 4
{ HIGH,  LOW, HIGH, HIGH,  LOW, HIGH, HIGH }, // 5
{ HIGH,  LOW, HIGH, HIGH, HIGH, HIGH, HIGH }, // 6
{ HIGH, HIGH, HIGH,  LOW,  LOW,  LOW, LOW  }, // 7
{ HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH }, // 8
{ HIGH, HIGH, HIGH, HIGH,  LOW, HIGH, HIGH }, // 9
{  LOW,  LOW,  LOW,  LOW,  LOW,  LOW,  LOW }  // all off
};
void setup() {
  for(int segment=firstSegmentPin;  segment<=lastSegmentPin;  segment++){pinMode(segment,OUTPUT);}
  for(int didgit=firstDidgitPin;    didgit<=lastDidgitPin;    didgit++) { 
    pinMode(      didgit,  OUTPUT);
    digitalWrite( didgit,  HIGH);
  }    
  Serial.begin(baudRate);
}
void drawDidgit(int number){
    for(int segment=firstSegmentPin; segment<=lastSegmentPin; segment++){digitalWrite(segment,aSegments[number][segment-firstSegmentPin]);}
    for(int segment=firstSegmentPin; segment<=lastSegmentPin; segment++){digitalWrite(segment,LOW);}
}
void loop() {
  if(Serial.available()){number=Serial.parseInt();}
  for(int didgit=firstDidgitPin; didgit<=lastDidgitPin; didgit++){
    digitalWrite(didgit,LOW);
    drawDidgit((number/pow10[didgit-firstDidgitPin])%10);
    digitalWrite(didgit,HIGH);
  }
}
